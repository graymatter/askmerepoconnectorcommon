package com.graymatter.askme.connector.common.bean;

import java.io.Serializable;
import java.util.Date;

import org.bson.Document;

import com.graymatter.askme.common.utility.ASKUtility;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;

public class AskMeLog extends Document implements Serializable{

	private static final long serialVersionUID = 1L;

	
	private String action;
	private Date date;
	private String description;
	private String exception;
	private String logNameConstant;
	
	
	public AskMeLog(String logNameConstant, String action, Date date, String description, String exception) {
		super();
		this.logNameConstant = logNameConstant;
		this.action = action;
		this.date = date;
		this.description = description;
		this.exception = exception;
	}
	
		
	public String getLogNameConstant() {
		return logNameConstant;
	}
	public void setLogNameConstant(String logNameConstant) {
		this.logNameConstant = logNameConstant;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getException() {
		return exception;
	}
	public void setException(String exception) {
		this.exception = exception;
	}
	
	public static Document createDBObject(AskMeLog askMeLog) {
		DBObject dbObj = (DBObject) JSON.parse(ASKUtility.ASKJson.GSON.toJson(askMeLog));
		Document document = new Document(dbObj.toMap());
		return document;
    }


	public static class ASKE_ME_LOG_ACTION {
		public static final String FSC_ACTION = "FSC_ACTION";
		public static final String DBC_ACTION = "DBC_ACTION";
	}
	public static class ASK_ME_LOG_DESC {
		public static final String RESULT_OK = "Action executed with success";
		public static final String RESULT_KO = "Problems occurred during action execution";
		
		public static final String INIT_LOCAL_OK = "Init Local Collection Action executed with success";
		public static final String INIT_LOCAL_KO = "Problems occurred during Init Local Collection Action";
		
		public static final String WRITE_REMOTE_OK = "Write Remote Collection Action executed with success";
		public static final String WRITE_REMOTE_KO = "Problems occurred during Write Remote Collection Action";
	}
	
}
