package com.graymatter.askme.connector.common.util.fingerPrint;

import java.io.FileInputStream;
import java.io.Serializable;

import org.apache.commons.codec.digest.DigestUtils;

import com.graymatter.askme.connector.common.exception.AskMeUtilityThrow;
import com.graymatter.askme.connector.common.util.constants.AskMeConnectorError.AskMeEncodingError;

public class AskMeFingerPrintGenerator implements Serializable{

	private static final long serialVersionUID = 1L;
	

	public AskMeFingerPrintGenerator() {
		super();
	}
	
	
	public static String generateFingerPrint(FileInputStream file) throws Exception, AskMeUtilityThrow {
		
		String fingerPrint = null;
		try{
			fingerPrint = DigestUtils.md5Hex(file);
		} catch (Exception e) {
			e.printStackTrace();
			throw new AskMeUtilityThrow(AskMeEncodingError.ENCODING_GENERIC);
		} 
		
		return fingerPrint;
	}
	
	public static String generateFingerPrint(String file) throws Exception, AskMeUtilityThrow {
		
		String fingerPrint = null;
		try{
			fingerPrint = DigestUtils.md5Hex(file);
		} catch (Exception e) {
			e.printStackTrace();
			throw new AskMeUtilityThrow(AskMeEncodingError.ENCODING_GENERIC);
		} 
		
		return fingerPrint;
	}
}
