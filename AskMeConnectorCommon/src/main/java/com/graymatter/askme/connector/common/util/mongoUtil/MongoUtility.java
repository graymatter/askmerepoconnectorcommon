package com.graymatter.askme.connector.common.util.mongoUtil;

import java.io.Serializable;

import org.bson.Document;

import com.graymatter.askme.common.exception.ASKPersistenceThrow;
import com.graymatter.askme.common.mongo.ASKMongoGeneral;
import com.graymatter.askme.connector.common.bean.AskMeLog;
import com.mongodb.client.MongoCollection;

public class MongoUtility implements Serializable{

	private static final long serialVersionUID = 1L;
	private static ASKMongoGeneral remoteConnectionManager;
	private static ASKMongoGeneral localConnectionManager;
	private static String collectionName;
	
	public MongoUtility(ASKMongoGeneral localConnectionManager){
		super();
		this.localConnectionManager = localConnectionManager;
	}	
	
	public MongoUtility(ASKMongoGeneral localConnectionManager,
						ASKMongoGeneral remoteConnectionManager,
					    String collectionName) {
		super();
		this.remoteConnectionManager = remoteConnectionManager;
		this.localConnectionManager = localConnectionManager;
		this.collectionName = collectionName;
	}

	public void fromRemoteToLocal() throws ASKPersistenceThrow, Exception {
		try{
			MongoCollection<Document> remoteContentTableCollection = remoteConnectionManager.getCollection(collectionName);
//			MongoCollection<Document> localContentTableCollection = localConnectionManager.getCollection(collectionName);
//			remoteContentTableCollection.drop();
//			localContentTableCollection.drop();
			if(remoteContentTableCollection.count() > 0){
				localConnectionManager.createCollection(remoteContentTableCollection, collectionName);
			}
		} 
		catch (ASKPersistenceThrow e) {
			throw e;
		}
		catch (Exception exc){
			throw exc;
		}
		finally{
			try {
				if(remoteConnectionManager != null) remoteConnectionManager.closeClient();
				if(localConnectionManager != null) localConnectionManager.closeClient();
			} catch (ASKPersistenceThrow e) {
				e.printStackTrace();
			}
		}
	}

	public void fromLocalToRemote() throws ASKPersistenceThrow, Exception {
		try{
			MongoCollection<Document> localContentTableCollection = localConnectionManager.getCollection(collectionName);
			if(localContentTableCollection.count() > 0){
				remoteConnectionManager.createCollection(localContentTableCollection, collectionName);
			}
		} 
		catch (ASKPersistenceThrow e) {
			throw e;
		}
		catch (Exception exc){
			throw exc;
		}
		finally{
			try {
				if(remoteConnectionManager != null) remoteConnectionManager.closeClient();
				if(localConnectionManager != null) localConnectionManager.closeClient();
			} catch (ASKPersistenceThrow e) {
				e.printStackTrace();
			}
		}
	}
	
	public void logTransformationReceipt(AskMeLog askMeLog) throws ASKPersistenceThrow, Exception {
		try{
			Document bsonDocument = AskMeLog.createDBObject(askMeLog);
			localConnectionManager.insert(askMeLog.getLogNameConstant(), bsonDocument);
		}catch (ASKPersistenceThrow e) {
			throw e;
		}catch (Exception exc){
			throw exc;
		}finally{
			try {
				if(localConnectionManager != null) localConnectionManager.closeClient();
			} catch (ASKPersistenceThrow e) {
				e.printStackTrace();
			}
		}
	}

}
