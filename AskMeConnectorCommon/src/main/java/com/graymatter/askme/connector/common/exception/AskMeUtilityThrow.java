package com.graymatter.askme.connector.common.exception;

@SuppressWarnings("serial")
public class AskMeUtilityThrow extends Throwable{
	
	public AskMeUtilityThrow() {
		super();
	}

	public AskMeUtilityThrow(String message, Throwable cause) {
		super(message, cause);
	}

	public AskMeUtilityThrow(String message) {
		super(message);
	}

	public AskMeUtilityThrow(Throwable cause) {
		super(cause);
	}
}
