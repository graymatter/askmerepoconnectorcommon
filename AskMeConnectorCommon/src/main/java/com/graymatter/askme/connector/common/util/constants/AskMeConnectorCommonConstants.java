package com.graymatter.askme.connector.common.util.constants;

public class AskMeConnectorCommonConstants {
	
	public static class XMLEnvelopeConstants{
		public static final String STARTER = 				"configuration";
		public static final String DB_CONN_ATTEMPS =		"dbConnectionAttemps";
		public static final String JOB_ENV = 				"job";
		public static final String LOCAL_DATABASE_ENV = 	"localDatabase";
		public static final String REMOTE_DATABASE_ENV = 	"remoteDatabase";
		public static final String SQL_ENV = 				"sqlDefinition";
		public static final String FILTER_ENV = 			"filterDefinition";
		public static final String HOSTNAME = 				"host";
		public static final String DBTYPE = 				"type";
		public static final String ACCESS = 				"access";
		public static final String DBNAME = 				"dbname";
		public static final String PORT = 					"port";
		public static final String USER = 					"user";
		public static final String PWD = 					"pwd";
	}

	public static class JobConstants{
		public static final String PATH =					"path";

		public static final String ATTR_NAME = 				"name";
		public static final String ATTR_DISABLED = 			"disabled";
		public static final String ATTR_INCREMENTAL =		"incremental";
		public static final String ATTR_SECTION_LENGTH =	"sectionLength";
		public static final int    MIN_SECTION_LENGTH =		1000;
		public static final int    MAX_SECTION_LENGTH =		5000;
		
		public static final String DROP_DBC_ROW_TABLE = 	"dropDBC_ROW";

		public static String[] getAttribute(){
			return new String[]{ATTR_NAME, ATTR_DISABLED, ATTR_INCREMENTAL, ATTR_SECTION_LENGTH};
		}
		
		public static class UtilityConstants{
			
			public static String ATTR_TAG = 			 "tag";
			public static String ATTR_TYPE = 			 "type";
			public static String ATTR_SEPARATOR = 		 "separator";
			public static String ATTR_FORMAT = 			 "format";
			public static final String FIELD =	 		 "field";
			public static final String FIELD_SEPARATOR = ",";
			
			public static String[] getAttribute(){
				return new String[]{ATTR_TYPE, ATTR_FORMAT, ATTR_SEPARATOR, ATTR_TAG};
			}
		}
	}
	
	

}
